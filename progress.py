"""
    papy-s-net: The neural network model to classify fragments of papyrus rewritten with PyTorch.
    Copyright (C) 2021 Group 1 - Project Management course

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


__author__ = ["Vu Lan Chi", "Pham Cong Danh", "Doan Phuoc Loc", "Bui Mai Nhi"]
__copyright__ = "Copyright 2021, Group 1 - Project Management course"
__license__ = "GPLv3"
__version__ = "1.0"
__maintainer__ = "Group 1 - Project Management course"
__email__ = ["chivu.minf20@iei.edu.vn", "danhpham.minf20@iei.edu.vn", "locdoan.minf20@iei.edu.vn", "nhibui.minf20@iei.edu.vn"]
__status__ = "Production"


import numpy as np
import sys
import time

class Progress:
    def __init__(self):
        self.times = []
        self.buffer_size = 10000
        self.lastTime = None 
        self.displayLastTime = None
        self.displayTickTime = 100  # ms

    def computeHMS(self, s):
        hours = (s / 60) // 60
        minutes = (s - hours * 60 * 60) // 60
        seconds = s - (minutes * 60 + hours * 60 * 60)

        return hours, minutes, seconds

    def tick(self, step, total_nb_steps, additional_info=None):

        currentTime = int(round(time.time() * 1000))

        # first iteration
        if self.lastTime == None:
            self.lastTime = currentTime
            self.displayLastTime = currentTime
            return

        elapsed = currentTime - self.lastTime
        self.lastTime = int(round(time.time() * 1000))

        self.times.append(elapsed)

        self.times = self.times[-self.buffer_size:]

        if (currentTime - self.displayLastTime) > self.displayTickTime:
            self.displayLastTime = int(round(time.time() * 1000))

            mean_step_time = np.mean(self.times)
            nb_steps_remaining = total_nb_steps - step
            time_remaining = mean_step_time * nb_steps_remaining
            percentage = round((step / total_nb_steps) * 100, 2)
            s_remaining = int(time_remaining / 1000)

            h, m, s = self.computeHMS(s_remaining)
            sys.stdout.write("%d/%d --  %d%% -- %dH - %dM - %dS \r" % (step, total_nb_steps, percentage, h, m, s))

