"""
    papy-s-net: The neural network model to classify fragments of papyrus rewritten with PyTorch.
    Copyright (C) 2021 Group 1 - Project Management course

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


__author__ = ["Vu Lan Chi", "Pham Cong Danh", "Doan Phuoc Loc", "Bui Mai Nhi"]
__copyright__ = "Copyright 2021, Group 1 - Project Management course"
__license__ = "GPLv3"
__version__ = "1.0"
__maintainer__ = "Group 1 - Project Management course"
__email__ = ["chivu.minf20@iei.edu.vn", "danhpham.minf20@iei.edu.vn", "locdoan.minf20@iei.edu.vn", "nhibui.minf20@iei.edu.vn"]
__status__ = "Production"


import torch
from torch import nn
from torchvision import models
import math
import torch.nn.functional as F


class SamePad2d(nn.Module):
    def __init__(self, kernel_size, stride):
        super(SamePad2d, self).__init__()
        self.kernel_size = torch.nn.modules.utils._pair(kernel_size)
        self.stride = torch.nn.modules.utils._pair(stride)

    def forward(self, input):
        in_width = input.size()[2]
        in_height = input.size()[3]
        out_width = math.ceil(float(in_width) / float(self.stride[0]))
        out_height = math.ceil(float(in_height) / float(self.stride[1]))
        pad_along_width = ((out_width - 1) * self.stride[0] + self.kernel_size[0] - in_width)
        pad_along_height = ((out_height - 1) * self.stride[1] + self.kernel_size[1] - in_height)
        pad_left = math.floor(pad_along_width / 2)
        pad_top = math.floor(pad_along_height / 2)
        pad_right = pad_along_width - pad_left
        pad_bottom = pad_along_height - pad_top
        return F.pad(input, (pad_left, pad_right, pad_top, pad_bottom), 'constant', 0)


class papy_s_net_branch (nn.Module):
    def __init__(self, dropout=None):
        super().__init__()
        self.padding = SamePad2d(kernel_size=3, stride=1)
        self.conv1 = nn.Conv2d(3, 64, kernel_size=3, stride=1, padding="same")
        self.conv2 = nn.Conv2d(64, 64, kernel_size=3, stride=1, padding="same")
        self.conv3 = nn.Conv2d(64, 128, kernel_size=3, stride=1, padding="same")
        self.conv4 = nn.Conv2d(128, 128, kernel_size=3, stride=1, padding="same")
        self.conv5 = nn.Conv2d(128, 256, kernel_size=3, stride=1, padding="same")
        self.conv6 = nn.Conv2d(256, 256, kernel_size=3, stride=1, padding="same")
        self.conv7 = nn.Conv2d(256, 512, kernel_size=3, stride=1, padding="same")
        self.conv8 = nn.Conv2d(512, 512, kernel_size=3, stride=1, padding="same")
        self.pooling = nn.MaxPool2d(2)
        self.dropout = nn.Dropout2d(dropout) if dropout is not None else None
        self.relu = nn.ReLU()

    def forward(self, x):
        out = self.relu(self.conv1(x))
        out = self.dropout(out) if self.dropout is not None else out

        out = self.relu(self.conv2(out))
        out = self.pooling(self.dropout(out) if self.dropout is not None else out)

        out = self.relu(self.conv3(out))
        out = self.dropout(out) if self.dropout is not None else out

        out = self.relu(self.conv4(out))
        out = self.pooling(self.dropout(out) if self.dropout is not None else out)

        out = self.relu(self.conv5(out))
        out = self.dropout(out) if self.dropout is not None else out

        out = self.relu(self.conv6(out))
        out = self.pooling(self.dropout(out) if self.dropout is not None else out)

        out = self.relu(self.conv7(out))
        out = self.dropout(out) if self.dropout is not None else out

        out = self.relu(self.conv8(out))
        out = self.dropout(out) if self.dropout is not None else out

        out = torch.flatten(out, 1)
        return out


class Identity(nn.Module):
    def __init__(self):
        super(Identity, self).__init__()

    def forward(self, x):
        return x


class Siamese(nn.Module):
    def __init__(self, network_type, dropout=None):
        super(Siamese, self).__init__()
        assert network_type in ["vgg", "papy", "resnet50"]

        if network_type == 'resnet50':
            self.feature_extraction = models.resnet50(pretrained=True)
            #simple way to eliminate the layer that we dont need (also have other ways depend on the purposes
            # and models, search transfer learning for more detail.
            self.feature_extraction.fc = Identity()
            self.fc0 = nn.Linear(2048, 512)
        elif network_type == 'vgg':
            self.feature_extraction = models.vgg16(pretrained=True)
            #other way to make transfer learning for vgg16 network.
            #self.feature_extraction.classifier = nn.Sequential(*[self.feature_extraction.classifier[i] for i in range(6)])
            self.feature_extraction.classifier = Identity()
            self.fc0 = nn.Linear(512, 512)
        else:
            self.feature_extraction = papy_s_net_branch(dropout=dropout)
            self.fc0 = nn.Linear(32768, 512)

        self.relu = nn.ReLU()
        self.fc1 = nn.Linear(512, 512)
        self.fc2 = nn.Linear(512, 1)
        self.sigmoid = nn.Sigmoid()

        #initialize weights
        def xavier(param):
            torch.nn.init.xavier_uniform(param)

        def weights_init(m):
            if isinstance(m, nn.Conv2d):
                xavier(m.weight.data)
                if m.bias is not None:
                    m.bias.data.zero_()
            elif isinstance(m, nn.Linear):
                m.weight.data.normal_(0, 0.01)
                m.bias.data.zero_()

        self.feature_extraction.apply(weights_init)

    def forward(self, x):
        x_l, x_r = x[0], x[1]

        left_embeddings = self.feature_extraction(x_l)
        right_embeddings = self.feature_extraction(x_r)

        L1_distance = torch.abs(torch.subtract(left_embeddings, right_embeddings))

        x = self.relu(self.fc0(L1_distance))
        x = self.relu(self.fc1(x))
        prediction = self.sigmoid(self.fc2(x))

        return prediction
