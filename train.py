"""
    papy-s-net: The neural network model to classify fragments of papyrus rewritten with PyTorch.
    Copyright (C) 2021 Group 1 - Project Management course

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


__author__ = ["Vu Lan Chi", "Pham Cong Danh", "Doan Phuoc Loc", "Bui Mai Nhi"]
__copyright__ = "Copyright 2021, Group 1 - Project Management course"
__license__ = "GPLv3"
__version__ = "1.0"
__maintainer__ = "Group 1 - Project Management course"
__email__ = ["chivu.minf20@iei.edu.vn", "danhpham.minf20@iei.edu.vn", "locdoan.minf20@iei.edu.vn", "nhibui.minf20@iei.edu.vn"]
__status__ = "Production"

import argparse
import datetime
import io
import os
import pickle

import cv2
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sn
import sklearn.metrics
import torch
import torch.nn.functional as F
import torch.utils.data as data
from torch.utils.tensorboard import SummaryWriter
from torchvision import transforms

import batch_utils
import data_loader
import utils
from models import Siamese

now = datetime.datetime.now()
torch.backends.cudnn.benchmark = False
os.environ['CUDA_LAUNCH_BLOCKING'] = "1"
transforms = transforms.Compose([transforms.ToPILImage(), transforms.ColorJitter(contrast=0.1)])

# Pytorch version used: 1.9.1

def typeDir(str):
    if not os.path.isdir(str):
        raise argparse.ArgumentTypeError("{0} is not a directory.".format(str))
    return str


argParser = argparse.ArgumentParser(description="Training")
argParser.add_argument(
    "-g", "--gpuNb", type=int, default=0, help="gpu id for training"
)
argParser.add_argument(
    "-da",
    "--dataset",
    type=typeDir,
    default=os.path.join(os.getcwd(), "pickleFiles"),
    help="path to dataset (directory containing .pickle files)",
)
argParser.add_argument(
    "-d", "--saveDir", type=str, default=os.path.join(os.getcwd(), "logs"), help="where to save the training"
)
argParser.add_argument(
    "-f",
    "--fineTune",
    type=str,
    default=None,
    help="if fine tuning, path to .pt starting file",
)

args = argParser.parse_args()
saveDir = args.saveDir
if not os.path.exists(saveDir):
    os.mkdir(saveDir)
logdir = os.path.join(str(args.saveDir), "scalars", now.strftime("%Y%m%d-%H%M%S"))

os.environ["CUDA_VISIBLE_DEVICES"] = str(args.gpuNb)

with open(os.path.join(args.dataset, "dataset_train.pickle"), "rb") as f:
    dataset_train = pickle.load(f)

with open(os.path.join(args.dataset, "dataset_val.pickle"), "rb") as f:
    dataset_val = pickle.load(f)

sample_im_shape = dataset_train[list(dataset_train.keys())[0]][0].shape
input_shape = (sample_im_shape[1], sample_im_shape[2], sample_im_shape[3])

currentEpoch = 0

# === Training Hyperparameters ===
batch_size = 128
nb_epochs = 1000
learning_rate = 1e-6  # overriden if the LearningRateScheduler is active
network_type = "resnet50"
# ===============================


if args.fineTune is not None:
    model = torch.load(args.fineTune)
else:
    model = Siamese(network_type)

dataset_train_sampler = data_loader.DataLoader(args.dataset,
                                               batch_size,
                                               input_shape,
                                               adaptive_distribution=False,
                                               mode='train',
                                               transform=transforms)

dataset_val_sampler = data_loader.DataLoader(args.dataset,
                                             batch_size,
                                             input_shape,
                                             adaptive_distribution=False,
                                             mode='val',
                                             transform=transforms)

train_dataloader = data.DataLoader(dataset_train_sampler,
                                   batch_size=1,
                                   num_workers=0,
                                   pin_memory=True,
                                   shuffle=True,
                                   collate_fn=data_loader.DataLoader.collate_fn)

val_dataloader = data.DataLoader(dataset_val_sampler,
                                 batch_size=1,
                                 num_workers=0,
                                 pin_memory=True,
                                 shuffle=True,
                                 collate_fn=data_loader.DataLoader.collate_fn)

device = 'cuda:' + str(args.gpuNb) if torch.cuda.is_available() else 'cpu'

writer = SummaryWriter(log_dir=logdir)


class tensorboard_plot_callback:
    def __init__(self, mode):
        super().__init__()
        self.mode = mode
        assert self.mode in [
            "histogram",
            "confusion",
            "precision_recall",
            "ROC",
            "map",
            "n_way"
        ]

    def make_map_tests(self):
        n_values = [x for x in range(2, 20, 1)]
        nb_trials_per_n = 10

        results = []
        for n in n_values:
            correct = 0
            map = []

            for t in range(nb_trials_per_n):
                n_way_pairs, n_way_targets = batch_utils.get_map_task_val(n, dataset_val, input_shape)
                predictions = []
                for n_way_pair in n_way_pairs:
                    n_way_pair = np.array(n_way_pair, dtype=np.float32)
                    n_way_pair = n_way_pair[:, np.newaxis, :, :]
                    predictions.append(model(torch.from_numpy(n_way_pair).to(device)).item())

                ap = utils.my_average_precision_at_k(n_way_targets, predictions, n)
                map.append(ap)

            results.append(np.mean(map))

        return results, n_values

    def get_map_curve_im(self):
        results, n_values = self.make_map_tests()

        plt.cla()
        plt.figure(figsize=(10, 7))
        plt.plot(n_values, results)
        plt.xlabel("n")
        plt.ylabel("map")
        io_buf = io.BytesIO()
        plt.savefig(io_buf, format='png', dpi=180)
        io_buf.seek(0)
        img_arr = np.frombuffer(io_buf.getvalue(), dtype=np.uint8)
        io_buf.close()
        img = cv2.imdecode(img_arr, 1)
        img = img[:, :, ::-1].transpose(2, 0, 1)
        img = np.ascontiguousarray(img)
        tf_img = torch.from_numpy(img)
        plt.close()

        return tf_img

    def make_n_way_tests(self):
        n_values = [x for x in range(2, 20, 1)]
        nb_trials_per_n = 10

        results = []
        for n in n_values:
            correct = 0
            for t in range(nb_trials_per_n):
                n_way_pairs, n_way_targets = batch_utils.get_n_way_task_val(n, dataset_val, input_shape)
                predictions = []
                for n_way_pair in n_way_pairs:
                    n_way_pair = np.array(n_way_pair, dtype=np.float32)
                    n_way_pair = n_way_pair[:, np.newaxis, :, :]
                    predictions.append(model(torch.from_numpy(n_way_pair).to(device)).item())
                index_max = np.argmax(predictions)
                if int(n_way_targets[index_max]) == 1:
                    correct += 1
            results.append((correct / (nb_trials_per_n * 1.0)))

        return results, n_values

    def get_n_way_curve_im(self):
        results, n_values = self.make_n_way_tests()

        plt.cla()
        plt.figure(figsize=(10, 7))
        plt.plot(n_values, results)
        plt.xlabel("n")
        plt.ylabel("correct percentage")
        io_buf = io.BytesIO()
        plt.savefig(io_buf, format='png', dpi=180)
        io_buf.seek(0)
        img_arr = np.frombuffer(io_buf.getvalue(), dtype=np.uint8)
        io_buf.close()
        img = cv2.imdecode(img_arr, 1)
        img = img[:, :, ::-1].transpose(2, 0, 1)
        img = np.ascontiguousarray(img)
        tf_img = torch.from_numpy(img)
        plt.close()

        return tf_img

    def get_histogram_im(self, predictions, targets):
        predictions_true = predictions[targets == 1]
        predictions_false = predictions[targets == 0]

        plt.cla()
        fig, ax = plt.subplots(1, 1)
        ax.hist(predictions_false, label="predictions false")
        ax.hist(predictions_true, label="predictions true", fc=(1, 0, 0, 0.5))

        ax.set_title("histogram of result")
        ax.set_xlabel("score")
        ax.set_ylabel("nb scores")
        plt.legend()
        # plt.show()

        io_buf = io.BytesIO()
        plt.savefig(io_buf, format='png', dpi=180)
        io_buf.seek(0)
        img_arr = np.frombuffer(io_buf.getvalue(), dtype=np.uint8)
        io_buf.close()
        img = cv2.imdecode(img_arr, 1)
        img = img[:, :, ::-1].transpose(2, 0, 1)
        img = np.ascontiguousarray(img)
        tf_img = torch.from_numpy(img)
        plt.close()

        return tf_img

    def get_confusion_matrix_im(self, predictions, targets, normalized=False):
        threshold = 0.5

        if normalized:
            confusion_mat = sklearn.metrics.confusion_matrix(
                targets,
                [1 if x > threshold else 0 for x in predictions],
                normalize="pred",
            )
        else:
            confusion_mat = sklearn.metrics.confusion_matrix(
                targets, [1 if x > threshold else 0 for x in predictions]
            )

        df_cm = pd.DataFrame(
            confusion_mat, index=["TN, FP", "FN, TP"], columns=[" ", " "]
        )
        plt.cla()
        plt.figure(figsize=(10, 7))
        if normalized:
            sn.heatmap(df_cm, annot=True, fmt="f")
        else:
            sn.heatmap(df_cm, annot=True, fmt="d")

        io_buf = io.BytesIO()
        plt.savefig(io_buf, format='png', dpi=180)
        io_buf.seek(0)
        img_arr = np.frombuffer(io_buf.getvalue(), dtype=np.uint8)
        io_buf.close()
        img = cv2.imdecode(img_arr, 1)
        img = img[:, :, ::-1].transpose(2, 0, 1)
        img = np.ascontiguousarray(img)
        tf_img = torch.from_numpy(img)
        plt.close()

        return tf_img

    def get_precision_recall_im(self, predictions, targets):

        precision, recall, thresholds = sklearn.metrics.precision_recall_curve(
            targets, predictions
        )
        plt.cla()
        plt.figure(figsize=(10, 7))
        plt.plot(recall, precision)
        plt.xlabel("Recall")
        plt.ylabel("precision")

        io_buf = io.BytesIO()
        plt.savefig(io_buf, format='png', dpi=180)
        io_buf.seek(0)
        img_arr = np.frombuffer(io_buf.getvalue(), dtype=np.uint8)
        io_buf.close()
        img = cv2.imdecode(img_arr, 1)
        img = img[:, :, ::-1].transpose(2, 0, 1)
        img = np.ascontiguousarray(img)
        tf_img = torch.from_numpy(img)
        plt.close()

        return tf_img

    def get_ROC_im(self, predictions, targets):

        fpr, tpr, thresholds = sklearn.metrics.roc_curve(targets, predictions)
        plt.cla()
        plt.figure(figsize=(10, 7))
        plt.plot(fpr, tpr)
        plt.xlabel("False positive rate")
        plt.ylabel("True positive rate")

        io_buf = io.BytesIO()
        plt.savefig(io_buf, format='png', dpi=180)
        io_buf.seek(0)
        img_arr = np.frombuffer(io_buf.getvalue(), dtype=np.uint8)
        io_buf.close()
        img = cv2.imdecode(img_arr, 1)
        img = img[:, :, ::-1].transpose(2, 0, 1)
        img = np.ascontiguousarray(img)
        tf_img = torch.from_numpy(img)
        plt.close()

        return tf_img

    def on_epoch_end(self, epoch, logs={}):
        batch_iter = iter(val_dataloader)
        pairs, targets = next(batch_iter)
        if 'cuda' in device:
            inputs = pairs.to(device)
        else:
            inputs = pairs

        predictions = model(inputs.squeeze(0))
        targets = targets.detach().cpu().numpy().reshape(-1, 1)
        predictions = predictions.detach().cpu().numpy()

        if self.mode == "histogram":
            tf_img = self.get_histogram_im(predictions, targets)
        elif self.mode == "confusion":
            tf_img = self.get_confusion_matrix_im(predictions, targets)
            tf_img_n = self.get_confusion_matrix_im(predictions, targets, normalized=True)
        elif self.mode == "precision_recall":
            tf_img = self.get_precision_recall_im(predictions, targets)
        elif self.mode == "ROC":
            tf_img = self.get_ROC_im(predictions, targets)
        elif self.mode == "n_way":
            tf_img = self.get_n_way_curve_im()
        elif self.mode == "map":
            tf_img = self.get_map_curve_im()

        writer.add_image(self.mode, tf_img, epoch)
        if self.mode == "confusion":
            writer.add_image(self.mode + "_normalized", tf_img_n, epoch)


class CurrentEpochCallback:
    global currentEpoch

    def on_epoch_end(self, epoch, logs={}):
        currentEpoch = epoch


nb_images_train = 0
for k, v in dataset_train.items():
    nb_images_train += len(v)

nb_images_val = 0
for k, v in dataset_val.items():
    nb_images_val += len(v)

callbacks = [
    CurrentEpochCallback(),
    tensorboard_plot_callback("confusion"),
    tensorboard_plot_callback("histogram"),
    tensorboard_plot_callback("precision_recall"),
    tensorboard_plot_callback("ROC"),
    tensorboard_plot_callback("map"),
]

thresholds = []
for i in range(0, 101, 1):
    thresholds.append(i / 100)


# define loss function
def compute_loss_accuracy(model, loader):
    loss = 0
    accuracy = 0
    totalinputs = 0
    totaloutput = 0
    batch_iter = iter(loader)
    for i in range(len(loader)):
        pairs, targets = next(batch_iter)

        inputs = pairs.to(device)
        labels = targets.to(device, dtype=torch.float32)

        output = model(inputs.squeeze(0))
        # calculate loss between predict and target using binary cross entropy
        loss += F.binary_cross_entropy(output, labels.view(-1, 1)).item()
        # accuracy += torch.sum(labels == torch.argmax(output, dim=1)).item()
        accuracy += torch.count_nonzero(labels.view(-1, 1) == (output > 0.5)).item()

        totalinputs += len(inputs)
        totaloutput += len(output)

    loss /= totalinputs
    accuracy /= totaloutput

    return loss, accuracy


# put model to device. device can be cpu, cuda:0, cuda:1 ,.., use torch.cuda.is_available()
# to check whether gpu is available or not
model.to(device)

# set optimizer for parameters in model
optimizer = torch.optim.Adam(model.parameters())

totalLoss = 0
totalAccuracy = 0
totalOutputLen = 0

for epoch in range(nb_epochs):
    # set train mode for model
    model.train()
    # get data from dataloader
    batch_iter = iter(train_dataloader)
    pairs, targets = next(batch_iter)
    optimizer.zero_grad()

    # put data to device
    pairs, targets = pairs.to(device), targets.to(device, dtype=torch.float32)
    # feed data to model
    preds = model(pairs.squeeze(0))
    # get loss value
    loss = F.binary_cross_entropy(preds, targets.view(-1, 1))
    print('loss', loss.item())
    # backward gradient
    loss.backward()
    optimizer.step()
    totalLoss += loss.item()
    totalAccuracy += torch.count_nonzero(targets.view(-1, 1) == (preds > 0.5)).item()
    totalOutputLen += len(preds)

    if np.mod(epoch, 5) == 0 and epoch > 0:
        model.eval()
        with torch.no_grad():
            tr_loss, tr_accuracy = totalLoss / (epoch + 1), totalAccuracy / totalOutputLen
            va_loss, va_accuracy = compute_loss_accuracy(model, val_dataloader)

        print("Epoch: {}/{}.. ".format(epoch + 1, nb_epochs),
              "Training Loss: {:.3f}.. ".format(tr_loss),
              "Validiton Loss: {:.3f}.. ".format(va_loss),
              "Training Accuracy: {:.3f}".format(tr_accuracy),
              "Validation Accuracy: {:.3f}".format(va_accuracy))
        for callback in callbacks:
            logs = dict(
                loss=tr_loss,
                acc=tr_accuracy,
                val_loss=va_loss,
                val_acc=va_accuracy,
            )
            callback.on_epoch_end(epoch, logs)
        writer.add_scalar('Loss/Train', tr_loss, epoch)
        writer.add_scalar('Accuracy/Train', tr_accuracy, epoch)
        writer.add_scalar('Loss/Validation', va_loss, epoch)
        writer.add_scalar('Accuracy/Validation', va_accuracy, epoch)
        writer.flush()
writer.close()
torch.save(model, os.path.join(str(args.saveDir), "model.pt"))
