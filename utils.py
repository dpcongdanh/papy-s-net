"""
    papy-s-net: The neural network model to classify fragments of papyrus rewritten with PyTorch.
    Copyright (C) 2021 Group 1 - Project Management course

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


__author__ = ["Vu Lan Chi", "Pham Cong Danh", "Doan Phuoc Loc", "Bui Mai Nhi"]
__copyright__ = "Copyright 2021, Group 1 - Project Management course"
__license__ = "GPLv3"
__version__ = "1.0"
__maintainer__ = "Group 1 - Project Management course"
__email__ = ["chivu.minf20@iei.edu.vn", "danhpham.minf20@iei.edu.vn", "locdoan.minf20@iei.edu.vn", "nhibui.minf20@iei.edu.vn"]
__status__ = "Production"

import os
import numpy as np
from torch import nn
from torchvision import transforms
from torch.utils.tensorboard import SummaryWriter

def lr_schedule(epoch):
    """
    Returns a custom learning rate that decreases as epochs progress.
    """
    learning_rate = 1e-3
    if epoch > 10:
        learning_rate = 5e-4
    if epoch > 25:
        learning_rate = 1e-4
    if epoch > 40:
        learning_rate = 5e-5

    SummaryWriter().add_scalar('learning rate', learning_rate, epoch)
    
    return learning_rate

data_augmentation = nn.Sequential(
    transforms.ColorJitter(contrast=0.1)
)


def my_average_precision_at_k(y_true, y_scores, k):  # targets, predictions
    y_scores_sorted, y_true_sorted = (
        np.array(list(t)) for t in zip(*sorted(zip(y_scores, y_true), reverse=True))
    )

    y_scores_sorted = y_scores_sorted[:k]
    y_true_sorted = y_true_sorted[:k]
    nb_true = (y_true_sorted == 1).sum()
    if nb_true == 0:
        return 0
    nb_true_seen = 1
    sum = 0
    for i in range(0, len(y_scores_sorted)):
        sum += y_true_sorted[i] * (nb_true_seen / (i + 1))
        if y_true_sorted[i] == 1:
            nb_true_seen += 1

    return sum / nb_true


def write_hisfrag_csv_and_gt(fragments_scores, path):
    ids_map = {}
    inverted_ids_map = {}
    for i, fragment_id in enumerate(list(fragments_scores.keys())):
        ids_map[fragment_id] = i
        inverted_ids_map[i] = fragment_id

    with open(os.path.join(path, "results.csv"), "w") as csv_file:
        for i in range(len(list(fragments_scores.keys()))):
            line = '' + str(i) + ','
            for j in range(len(list(fragments_scores.keys()))):
                if i == j:
                    line += str(0)
                else:
                    line += str(1 - np.mean(fragments_scores[inverted_ids_map[i]][inverted_ids_map[j]]))
                if j < len(list(fragments_scores.keys())) - 1:
                    line += ','
            csv_file.write(line)
            csv_file.write('\n')

    with open(os.path.join(path, "gt.csv"), "w") as csv_file:
        for i in range(len(inverted_ids_map)):
            csv_file.write(str(i) + ',' + str(inverted_ids_map[i].split('_')[0]))
            csv_file.write('\n')
