"""
    papy-s-net: The neural network model to classify fragments of papyrus rewritten with PyTorch.
    Copyright (C) 2021 Group 1 - Project Management course

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


__author__ = ["Vu Lan Chi", "Pham Cong Danh", "Doan Phuoc Loc", "Bui Mai Nhi"]
__copyright__ = "Copyright 2021, Group 1 - Project Management course"
__license__ = "GPLv3"
__version__ = "1.0"
__maintainer__ = "Group 1 - Project Management course"
__email__ = ["chivu.minf20@iei.edu.vn", "danhpham.minf20@iei.edu.vn", "locdoan.minf20@iei.edu.vn", "nhibui.minf20@iei.edu.vn"]
__status__ = "Production"

import os
import pickle
import torch
import numpy as np

from torch.utils.data import Dataset


class DataLoader(Dataset):

    def __init__(self,
                 data_path,
                 batch_size,
                 input_shape,
                 adaptive_distribution=False,
                 transform=None,
                 mode='train'):
        self.batch_size = batch_size
        self.input_shape = input_shape
        self.transform = transform
        self.adaptive_distribution = adaptive_distribution
        if mode == 'train':
            with open(os.path.join(data_path, "dataset_train.pickle"), "rb") as f:
                self.dataset = pickle.load(f)
        elif mode == 'val':
            with open(os.path.join(data_path, "dataset_val.pickle"), "rb") as f:
                self.dataset = pickle.load(f)
        self.progress = 0

    def __len__(self):
        return len(self.dataset)

    def __getitem__(self, index):
        self.progress += 1  # currentEpoch
        if self.adaptive_distribution:
            # epoch at which we go back to original distribution of data
            original_distribution = 0.004  # 9661 similar pairs, 2302664 dissimilar pairs
            starting_distribution = 0.9
            original_distribution_epoch = 400

            a = (original_distribution - starting_distribution) / original_distribution_epoch
            current_distribution = max(0.004, self.progress * a + starting_distribution)

            nb_similar = int(round(current_distribution * self.batch_size, 0))
            nb_dissimilar = int(round((1 - current_distribution) * self.batch_size, 0))

        else:
            nb_similar = self.batch_size // 2
            nb_dissimilar = self.batch_size // 2

        assert (nb_similar + nb_dissimilar) == self.batch_size

        self.pairs = [
            np.zeros(
                (self.batch_size, self.input_shape[2], self.input_shape[0], self.input_shape[1])
            ) for i in range(2)
        ]
        self.targets = np.zeros((self.batch_size,))
        self.labels = list(self.dataset.keys())

        for i in range(self.batch_size):
            current_label = np.random.choice(self.labels)

            if i < nb_similar:
                other_label = current_label
            else:
                other_label = np.random.choice(self.labels)
                while current_label == other_label:
                    other_label = np.random.choice(self.labels)

            self.targets[i] = int(current_label == other_label)
            current_im_idx = np.random.randint(0, len(self.dataset[current_label]))
            other_im_idx = np.random.randint(0, len(self.dataset[other_label]))
            if current_label == other_label:
                while current_im_idx == other_im_idx:
                    other_im_idx = np.random.randint(0, len(self.dataset[other_label]))

            current_im = self.dataset[current_label][current_im_idx]
            other_im = self.dataset[other_label][other_im_idx]

            if self.transform:
                current_im = np.array(self.transform(current_im.squeeze(0)))
                other_im = np.array(self.transform(other_im.squeeze(0)))

            current_im = current_im.reshape(self.input_shape)[:, :, ::-1].transpose(2, 0, 1)
            current_im = np.ascontiguousarray(current_im)

            other_im = other_im.reshape(self.input_shape)[:, :, ::-1].transpose(2, 0, 1)
            other_im = np.ascontiguousarray(other_im)

            self.pairs[0][i, :, :, :] = current_im/255
            self.pairs[1][i, :, :, :] = other_im/255

        return self.pairs, self.targets

    # yields data from data loader (get_item) in each iteration, convert numpy to torch tensor
    @staticmethod
    def collate_fn(batch):
        pairs, targets = zip(*batch)
        return torch.from_numpy(np.array(pairs, dtype=np.float32)), \
            torch.from_numpy(np.array(targets, dtype=np.float32))
